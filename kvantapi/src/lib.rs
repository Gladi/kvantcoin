use KvantcoinProtocol::kvantcoin_protocol_client::KvantcoinProtocolClient;
use KvantcoinProtocol::*;
use anyhow::Result;
use tonic::transport::Channel;

pub struct ToInfo {
    pub address_wallet: String,
    pub password: String
}

pub mod KvantcoinProtocol {
    tonic::include_proto!("kvantcoin_proto");
}

macro_rules! check_response {
    ($response:ident) => {
        if !$response.get_ref().is_successful {
            anyhow::bail!("Error message: {}", $response.get_ref().message_error);
        }
    };
}

pub struct KvantAPI {
    connect: KvantcoinProtocolClient<Channel>
}

impl KvantAPI {
    pub async fn connect(address: &str) -> Result<Self> {
        let connect: KvantcoinProtocolClient<_> = KvantcoinProtocolClient::connect(address.to_string()).await?;
        Ok(KvantAPI {
            connect: connect.into()
        })
    }

    pub async fn new_account(&mut self, address_wallet: &String, email: &String, password: &String) -> Result<()> {
        let request = tonic::Request::new(NewAccountRequest {
            address_wallet: address_wallet.clone(),
            email: email.clone(),
            password: password.clone()
        });

        let response = self.connect.new_account(request).await?;
        check_response!(response);

        Ok(())
    }

    pub async fn send_money(&mut self, to: &ToInfo, from_address_wallet: &String, value: &i32) -> Result<()> {
        if *value <= 0 {
            anyhow::bail!("Value zero or less than zero");
        }

        let request = tonic::Request::new(SendMoneyRequest {
            to: to.address_wallet.clone(),
            password: to.password.clone(),
            from: from_address_wallet.clone(),
            value: *value
        });

        let response = self.connect.send_money(request).await?;
        check_response!(response);

        Ok(())
    }

    pub async fn login_to_account(&mut self, email: &String, password: &String) -> Result<(bool, String, i32)> {
        if email.is_empty() || password.is_empty() {
            anyhow::bail!("Email or password is empty");
        }

        let request = tonic::Request::new(LoginToAccountRequest {
            email: email.clone(),
            password: password.clone()
        });

        let response = self.connect.login_to_account(request).await?;
        check_response!(response);

        Ok((response.get_ref().account_login_successful, response.get_ref().address_wallet_account.clone(), response.get_ref().balance_account))
    }
}

#[cfg(test)]
mod tests {
    use crate::ToInfo;

    use super::KvantAPI;

    #[tokio::test]
    async fn new_account() {
        let mut kvantapi = KvantAPI::connect("http://0.0.0.0:5421").await.unwrap();
        kvantapi.new_account(&"address_wallet_test".to_string(), &"email_test".to_string(), &"dsfdsfs".to_string()).await.unwrap();
    }

    #[tokio::test]
    async fn send_money() {
        let mut kvantapi = KvantAPI::connect("http://0.0.0.0:5421").await.unwrap();
        //kvantapi.new_account("address_wallet_test1".to_string(), "email_test1".to_string(), "dsfdsfs1".to_string()).await.unwrap();
        //kvantapi.new_account("address_wallet_test2".to_string(), "email_test2".to_string(), "dsfdsfs2".to_string()).await.unwrap();

        kvantapi.send_money(&ToInfo {
            address_wallet: "address_wallet_test".to_string(),
            password: "dsfdsfs".to_string()
        }, &"address_wallet_test2".to_string(), &46).await.unwrap();
    }

    #[tokio::test]
    async fn login_to_account() {
        let mut kvantapi = KvantAPI::connect("http://0.0.0.0:5421").await.unwrap();
        kvantapi.new_account(&"addrgsdgdssdvc".to_string(), &"emdsvsd".to_string(), &"dsfdsfs2".to_string()).await.unwrap();

        let result = kvantapi.login_to_account(&"emdsvsd".to_string(), &"dsfdsfs2".to_string()).await.unwrap();
        let (account_login_succ, _address_wallet, _balance) = result;
        assert_eq!(account_login_succ, true)
    }
}
