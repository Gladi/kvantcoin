mod detail {
    use crate::global;
    use anyhow::Result;
    use kvantapi::KvantAPI;
    use tokio::runtime::Runtime;

    pub(super) fn registration(
        address_wallet: &String,
        email: &String,
        password: &String,
    ) -> Result<()> {
        let rt = Runtime::new().unwrap();

        rt.block_on(async {
            let mut kvant = KvantAPI::connect(global::ADDRESS_TO_SERVER).await.unwrap();
            kvant
                .new_account(address_wallet, email, password)
                .await
                .unwrap();
        });

        Ok(())
    }
}

use crate::main_menu::MainMenu;
use crate::settings::*;
use crate::{global};
use cpp_core::{Ptr, StaticUpcast};
use qt_core::{slot, QBox, QObject, QPtr, QSortFilterProxyModel, QString, SlotNoArgs};
use qt_gui::QStandardItemModel;
use qt_ui_tools::ui_form;
use qt_widgets::{q_message_box, QLineEdit, QMessageBox, QPushButton, QWidget};
use std::rc::Rc;

#[ui_form("../ui/registration.ui")]
#[derive(Debug)]
pub struct Form {
    pub widget: QBox<QWidget>,
    email_line: QPtr<QLineEdit>,
    password_line: QPtr<QLineEdit>,
    address_wallet_line: QPtr<QLineEdit>,
    registration_button: QPtr<QPushButton>,
}

#[derive(Debug)]
#[allow(dead_code)]
pub struct Registration {
    form: Form,
    source_model: QBox<QStandardItemModel>,
    proxy_model: QBox<QSortFilterProxyModel>,
    main_menu_from: Rc<MainMenu>,
}

impl StaticUpcast<QObject> for Registration {
    unsafe fn static_upcast(ptr: Ptr<Self>) -> Ptr<QObject> {
        ptr.form.widget.as_ptr().static_upcast()
    }
}

impl Drop for Registration {
    fn drop(&mut self) {
        log::debug!("Dropping Registration with data `{:?}`!", self.form);
    }
}

impl Registration {
    pub fn new() -> Rc<Registration> {
        unsafe {
            let this = Rc::new(Registration {
                form: Form::load(),
                source_model: QStandardItemModel::new_0a(),
                proxy_model: QSortFilterProxyModel::new_0a(),
                main_menu_from: MainMenu::new(),
            });
            this.main_menu_from.set_parent(this.form.widget.as_ptr());
            this.init();
            this
        }
    }

    pub unsafe fn set_parent(self: &Rc<Self>, parent: Ptr<QWidget>) {
        self.form.widget.set_parent(parent);
    }

    unsafe fn init(self: &Rc<Self>) {
        self.form
            .registration_button
            .pressed()
            .connect(&self.slot_clicked_registration_button()); // BUG
    }

    #[slot(SlotNoArgs)]
    unsafe fn clicked_registration_button(self: &Rc<Self>) {
        log::debug!("run slot_clicked_registration in {}", file!());

        let address_wallet = global::convert_qstring_to_ref_string!(self, address_wallet_line);
        let email = global::convert_qstring_to_ref_string!(self, email_line);
        let password = global::convert_qstring_to_ref_string!(self, password_line);

        detail::registration(address_wallet, email, password).unwrap();

        let qmessage_box = QMessageBox::new();
        qmessage_box.set_text(&QString::from_std_str("Успешная регистрация!"));
        qmessage_box.set_icon(q_message_box::Icon::Information);
        qmessage_box.exec();

        let settings = Settings::new(email.clone(), password.clone());
        settings.save().unwrap();

        self.main_menu_from.update_information();
        self.main_menu_from.show();
        self.form.widget.hide();
    }

    pub fn show(self: &Rc<Self>) {
        unsafe {
            self.form.widget.show();
        }
    }
}
