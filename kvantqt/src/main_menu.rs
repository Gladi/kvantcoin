use crate::{global, settings::*};
use cpp_core::{Ptr, StaticUpcast};
use kvantapi::{KvantAPI, ToInfo};
use qt_core::{slot, QBox, QObject, QPtr, QSortFilterProxyModel, QString, SlotNoArgs};
use qt_gui::{QGuiApplication, QStandardItemModel};
use qt_ui_tools::ui_form;
use qt_widgets::{q_message_box, QLabel, QLineEdit, QMessageBox, QPushButton, QWidget};
use std::rc::Rc;

#[ui_form("../ui/main.ui")]
#[derive(Debug)]
pub struct Form {
    pub widget: QBox<QWidget>,
    value_line: QPtr<QLineEdit>,
    balance_label: QPtr<QLabel>,
    address_wallet_label: QPtr<QLabel>,
    from_line: QPtr<QLineEdit>,
    copy_address_wallet_button: QPtr<QPushButton>,
    update_information_button: QPtr<QPushButton>,
    send_kvant_button: QPtr<QPushButton>,
}

#[derive(Debug)]
#[allow(dead_code)]
pub struct MainMenu {
    form: Form,
    source_model: QBox<QStandardItemModel>,
    proxy_model: QBox<QSortFilterProxyModel>,
}

impl StaticUpcast<QObject> for MainMenu {
    unsafe fn static_upcast(ptr: Ptr<Self>) -> Ptr<QObject> {
        ptr.form.widget.as_ptr().static_upcast()
    }
}

impl Drop for MainMenu {
    fn drop(&mut self) {
        log::debug!("Dropping MainMenu with data `{:?}`!", self.form);
    }
}

impl MainMenu {
    pub fn new() -> Rc<MainMenu> {
        unsafe {
            let this = Rc::new(MainMenu {
                form: Form::load(),
                source_model: QStandardItemModel::new_0a(),
                proxy_model: QSortFilterProxyModel::new_0a(),
            });

            this.init();
            this
        }
    }

    pub unsafe fn set_parent(self: &Rc<Self>, parent: Ptr<QWidget>) {
        self.form.widget.set_parent(parent);
    }

    pub unsafe fn update_information(self: &Rc<Self>) {
        let settings = Settings::load().unwrap();
        let info = settings.get_info().unwrap();

        self.form
            .balance_label
            .set_text(&QString::from_std_str(format!(
                "Ваш баланс: {}",
                info.balance
            )));
        self.form
            .address_wallet_label
            .set_text(&QString::from_std_str(format!(
                "Адрес кошелька: {}",
                info.address_wallet
            )));
    }

    unsafe fn init(self: &Rc<Self>) {
        self.form
            .copy_address_wallet_button
            .pressed()
            .connect(&self.slot_clicked_copy_address_wallet_button());
        self.form
            .update_information_button
            .pressed()
            .connect(&self.slot_clicked_update_information_button());
        self.form
            .send_kvant_button
            .pressed()
            .connect(&self.slot_clicked_send_kvant_button());
    }

    #[slot(SlotNoArgs)]
    unsafe fn clicked_send_kvant_button(self: &Rc<Self>) {
        log::debug!("run clicked_send_kvant_button in {}", file!());
        let settings = Settings::load().unwrap();
        let info = settings.get_info().unwrap();
        let rt = tokio::runtime::Runtime::new().unwrap();
        let value = self.form.value_line.text().to_int_0a();

        if value > info.balance {
            let qmessage_box = QMessageBox::new();
            qmessage_box.set_text(&QString::from_std_str("Нехватка баланса для аккаунта!"));
            qmessage_box.set_icon(q_message_box::Icon::Critical);
            qmessage_box.exec();
            return;
        }

        if value == 0 {
            let qmessage_box = QMessageBox::new();
            qmessage_box.set_text(&QString::from_std_str(
                "Вы неправильно набрали label перевода!",
            ));
            qmessage_box.set_icon(q_message_box::Icon::Critical);
            qmessage_box.exec();
            return;
        }

        rt.block_on(async move {
            let mut kvant = KvantAPI::connect(global::ADDRESS_TO_SERVER).await.unwrap();
            if kvant
                .send_money(
                    &ToInfo {
                        address_wallet: info.address_wallet,
                        password: info.password,
                    },
                    &self.form.from_line.text().to_std_string(),
                    &value,
                )
                .await
                .is_err()
            {
                let qmessage_box = QMessageBox::new();
                qmessage_box.set_text(&QString::from_std_str("Неверный адрес кошелька!"));
                qmessage_box.set_icon(q_message_box::Icon::Critical);
                qmessage_box.exec();
            } else {
                let qmessage_box = QMessageBox::new();
                qmessage_box.set_text(&QString::from_std_str("Всё прошло успешно!"));
                qmessage_box.set_icon(q_message_box::Icon::Information);
                qmessage_box.exec();
            }
        });

        self.update_information();
    }

    #[slot(SlotNoArgs)]
    unsafe fn clicked_update_information_button(self: &Rc<Self>) {
        log::debug!("run clicked_update_information_button in {}", file!());
        self.update_information();
    }

    #[slot(SlotNoArgs)]
    unsafe fn clicked_copy_address_wallet_button(self: &Rc<Self>) {
        log::debug!("run clicked_copy_address_wallet_button in {}", file!());
        self.update_information();

        let settings = Settings::load().unwrap();
        let info = settings.get_info().unwrap();

        let clipboard = QGuiApplication::clipboard();
        clipboard.set_text_1a(&QString::from_std_str(&info.address_wallet));
    }

    pub fn show(self: &Rc<Self>) {
        unsafe {
            self.form.widget.show();
        }
    }
}
