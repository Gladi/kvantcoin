mod detail {
    use crate::global;
    use anyhow::Result;
    use kvantapi::KvantAPI;
    use tokio::runtime::Runtime;

    pub fn login(email: &String, password: &String) -> Result<()> {
        let rt = Runtime::new().unwrap();

        rt.block_on(async {
            let mut kvant = KvantAPI::connect(global::ADDRESS_TO_SERVER).await.unwrap();
            kvant.login_to_account(email, password).await.unwrap();
        });

        Ok(())
    }
}

use crate::registration::Registration;
use crate::settings::Settings;
use crate::{global::convert_qstring_to_ref_string, main_menu::MainMenu};
use cpp_core::{Ptr, StaticUpcast};
use qt_core::{slot, QBox, QObject, QPtr, QSortFilterProxyModel, QString, SlotNoArgs};
use qt_gui::QStandardItemModel;
use qt_ui_tools::ui_form;
use qt_widgets::{q_message_box, QLineEdit, QMessageBox, QPushButton, QWidget};
use std::rc::Rc;

#[ui_form("../ui/login.ui")]
#[derive(Debug)]
struct Form {
    widget: QBox<QWidget>,
    login_button: QPtr<QPushButton>,
    email_line: QPtr<QLineEdit>,
    password_line: QPtr<QLineEdit>,
    registration_button: QPtr<QPushButton>,
}

#[derive(Debug)]
#[allow(dead_code)]
pub struct Login {
    form: Form,
    source_model: QBox<QStandardItemModel>,
    proxy_model: QBox<QSortFilterProxyModel>,
    registration_form: Rc<Registration>,
    main_menu_from: Rc<MainMenu>,
}

impl StaticUpcast<QObject> for Login {
    unsafe fn static_upcast(ptr: Ptr<Self>) -> Ptr<QObject> {
        ptr.form.widget.as_ptr().static_upcast()
    }
}

impl Drop for Login {
    fn drop(&mut self) {
        log::debug!("Dropping Login with data `{:?}`!", self.form);
    }
}

impl Login {
    pub fn new() -> Rc<Self> {
        unsafe {
            let this = Rc::new(Login {
                form: Form::load(),
                source_model: QStandardItemModel::new_0a(),
                proxy_model: QSortFilterProxyModel::new_0a(),
                registration_form: Registration::new(),
                main_menu_from: MainMenu::new(),
            });
            this.registration_form.set_parent(this.form.widget.as_ptr());
            this.main_menu_from.set_parent(this.form.widget.as_ptr());
            this.init();
            this
        }
    }

    unsafe fn init(self: &Rc<Self>) {
        self.form
            .login_button
            .clicked()
            .connect(&self.slot_clicked_login());
        self.form
            .registration_button
            .clicked()
            .connect(&self.slot_clicked_registration());
    }

    #[slot(SlotNoArgs)]
    unsafe fn clicked_login(self: &Rc<Self>) {
        log::debug!("run slot_clicked_login in {}", file!());

        let email = convert_qstring_to_ref_string!(self, email_line);
        let password = convert_qstring_to_ref_string!(self, password_line);
        detail::login(email, password).unwrap();

        let qmessage_box = QMessageBox::new();
        qmessage_box.set_text(&QString::from_std_str("Успешный вход в аккаунт!"));
        qmessage_box.set_icon(q_message_box::Icon::Information);
        qmessage_box.exec();

        let settings = Settings::new(email.clone(), password.clone());
        settings.save().unwrap();

        self.main_menu_from.update_information();
        self.main_menu_from.show();
        self.form.widget.hide();
    }

    #[slot(SlotNoArgs)]
    unsafe fn clicked_registration(self: &Rc<Self>) {
        log::debug!("run slot_clicked_registration in {}", file!());

        self.registration_form.show();
        self.form.widget.hide();
    }

    pub fn show(self: &Rc<Self>) {
        unsafe {
            self.form.widget.show();
        }
    }
}
