#![windows_subsystem = "windows"]

mod global;
mod login;
mod main_menu;
mod registration;
mod settings;

use crate::main_menu::*;
use qt_widgets::QApplication;
use settings::Settings;

fn main() {
    dotenv::dotenv().ok();
    env_logger::init();
    QApplication::init(|_| {
        if !Settings::is_exists().unwrap() {
            let login_widget = login::Login::new();
            login_widget.show();
            unsafe { QApplication::exec() }
        } else {
            let main_menu = MainMenu::new();
            main_menu.show();
            unsafe { QApplication::exec() }
        }
    });
}
