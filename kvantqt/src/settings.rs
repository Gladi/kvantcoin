use crate::global;
use anyhow::Result;
use kvantapi::KvantAPI;
use serde::{Deserialize, Serialize};
use std::env;
use std::fs::{File, OpenOptions};
use std::io::{Read, Write};
use std::path::PathBuf;

pub fn get_path_to_settings() -> Result<PathBuf> {
    let mut path_to_program = env::current_exe()?;
    path_to_program.pop();
    let path = path_to_program.join("acccount.kv");
    Ok(path)
}

pub struct AccountInfo {
    pub email: String,
    pub password: String,
    pub address_wallet: String,
    pub balance: i32,
}

#[derive(Deserialize, Serialize)]
pub struct Settings {
    email: String,
    password: String,
}

impl Settings {
    pub fn get_info(&self) -> Result<AccountInfo> {
        let rt = tokio::runtime::Runtime::new()?;
        let mut address = String::new();
        let mut balance = 0i32;

        rt.block_on(async {
            let mut kvant = KvantAPI::connect(global::ADDRESS_TO_SERVER).await.unwrap();
            let (login_successful, address_wallet, balance_account) = kvant
                .login_to_account(&self.email, &self.password)
                .await
                .unwrap();

            assert!(login_successful);
            address = address_wallet;
            balance = balance_account;
        });

        Ok(AccountInfo {
            email: self.email.clone(),
            password: self.password.clone(),
            address_wallet: address,
            balance,
        })
    }

    pub fn new(email: String, password: String) -> Self {
        Settings { email, password }
    }

    pub fn save(&self) -> Result<()> {
        let path = get_path_to_settings()?;
        let json = serde_json::to_string(self)?;
        //log::warn!("PATH: {}", path.display());
        let mut file = OpenOptions::new()
            .read(true)
            .write(true)
            .create_new(true)
            .open(path)?;

        file.write_all(json.as_bytes())?;
        Ok(())
    }

    pub fn load() -> Result<Self> {
        let path = get_path_to_settings()?;
        let mut file = File::open(path)?;
        let mut buffer = String::new();
        file.read_to_string(&mut buffer)?;

        let result: Settings = serde_json::from_str(&buffer)?;
        Ok(result)
    }

    pub fn is_exists() -> Result<bool> {
        let path = get_path_to_settings()?;
        Ok(std::fs::metadata(path).is_ok())
    }
}
