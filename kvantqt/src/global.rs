pub const ADDRESS_TO_SERVER: &str = "http://80.87.198.236:5421";

macro_rules! convert_qstring_to_ref_string {
    ($self:ident, $x:ident) => {
        &$self.form.$x.text().to_std_string()
    };
}

pub(crate) use convert_qstring_to_ref_string;
