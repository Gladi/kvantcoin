use anyhow::{Ok, Result};
use console::Term;
use dialoguer::theme::ColorfulTheme;
use dialoguer::{Input, Password, Select};
use kvantapi::{KvantAPI, ToInfo};
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::fs::OpenOptions;
use std::io::prelude::*;

const ADDRESS_TO_SERVER: &str = "http://37.113.33.83:5421";

#[derive(Deserialize, Serialize)]
pub struct AccountInfo {
    email: String,
    address_wallet: String,
    password: String,
    balance: i32,
}

impl AccountInfo {
    pub fn save(&self) -> Result<()> {
        let json = serde_json::to_string(self)?; // TODO открыть или создать файл
        let mut file = OpenOptions::new()
            .read(true)
            .write(true)
            .create(true)
            .open("acccount.kv")?;

        file.write_all(json.as_bytes())?;
        Ok(())
    }

    pub fn load() -> Result<Self> {
        let mut file = File::open("acccount.kv")?;
        let mut buffer = String::new();
        file.read_to_string(&mut buffer)?;

        let result: AccountInfo = serde_json::from_str(&buffer)?;
        Ok(result)
    }

    pub fn is_exists() -> bool {
        std::fs::metadata("acccount.kv").is_ok()
    }

    pub async fn update_balance(&mut self) -> Result<()> {
        let mut kvantapi = KvantAPI::connect(ADDRESS_TO_SERVER).await?;
        self.balance = kvantapi
            .login_to_account(&self.email, &self.password)
            .await?
            .2;
        Ok(())
    }

    pub async fn display_info(&mut self) -> Result<String> {
        self.update_balance().await?;

        let mut info = String::new();
        info.push_str(format!("Адрес аккаунта: {}\n", self.address_wallet).as_str());
        info.push_str(format!("Баланс: {}\n", self.balance).as_str());
        info.push_str(format!("Email: {}", self.email).as_str());

        Ok(info)
    }
}

pub async fn registration() -> Result<AccountInfo> {
    let email: String = Input::new()
        .with_prompt("Укажите ваш email")
        .interact_text()?;

    let address_wallet: String = Input::new()
        .with_prompt("Укажите какой хотите адрес кошелька")
        .interact_text()?;

    let password = Password::new()
        .with_prompt("Укажите пароль")
        .with_confirmation("Подтверждение пароля", "Пароли не совпадают!")
        .interact()?;

    let mut kvantapi = KvantAPI::connect(ADDRESS_TO_SERVER).await?;
    kvantapi
        .new_account(&address_wallet, &email, &password)
        .await?;

    Ok(AccountInfo {
        email: email.clone(),
        address_wallet: address_wallet,
        password: password.clone(),
        balance: kvantapi.login_to_account(&email, &password).await?.2,
    })
}

pub async fn login() -> Result<AccountInfo> {
    let mut kvantapi = KvantAPI::connect(ADDRESS_TO_SERVER).await?;

    let email: String = Input::new()
    .with_prompt("Укажите ваш email")
    .interact_text()?;

    let password: String = Input::new()
    .with_prompt("Укажите ваш пароль")
    .interact_text()?;

    let info = kvantapi.login_to_account(&email, &password).await?;
    
    Ok(AccountInfo {
        email: email.clone(),
        address_wallet: info.1,
        password: password.clone(),
        balance: info.2,
    })
}

async fn send_kvant() -> Result<()> {
    let account_info = AccountInfo::load()?;
    let mut kvantapi = KvantAPI::connect(ADDRESS_TO_SERVER).await?;

    let from_address_wallet: String = Input::new()
        .with_prompt("Укажите адрес кошелька")
        .interact_text()?;

    let value: i32 = Input::new()
        .with_prompt("Сколько надо перевести?")
        .interact_text()?;

    kvantapi
        .send_money(
            &ToInfo {
                address_wallet: account_info.address_wallet,
                password: account_info.password,
            },
            &from_address_wallet,
            &value,
        )
        .await?;

    Ok(())
}

pub async fn shell_account() -> Result<()> {
    loop {
        println!("Что вы хотите сделать с аккаунтом?");
        let items = vec!["Получить информацию про аккаунт", "Перевести кванты"];

        let selection = Select::with_theme(&ColorfulTheme::default())
            .items(&items)
            .default(1)
            .interact_on_opt(&Term::stderr())?;

        match selection {
            Some(index) => {
                if items[index] == "Получить информацию про аккаунт" {
                    let mut account_info = AccountInfo::load()?;
                    account_info.update_balance().await?;

                    println!("Информация про ваш аккаунт:");
                    println!("{}", account_info.display_info().await?);
                } else if items[index] == "Перевести кванты" {
                    send_kvant().await?;
                }
            }
            None => panic!("Вы не выбрали действие!"),
        }
    }
}
