mod account;

use anyhow::Result;
use colored::Colorize;
use console::Term;
use dialoguer::{theme::ColorfulTheme, Select};

#[tokio::main]
async fn main() -> Result<()> {
    println!("Здрайствуйте, это консольный клиент для Kvantcoin");

    if !account::AccountInfo::is_exists() {
        println!("Вам нужно войти или зарегистрироваться в аккаунт");
        println!("{}", "Выберите действие:".bold());

        let items = vec!["Регистрация", "Вход"];
        let selection = Select::with_theme(&ColorfulTheme::default())
            .items(&items)
            .default(0)
            .interact_on_opt(&Term::stderr())?;

        match selection {
            Some(index) => {
                if items[index] == "Вход" {
                    let account_info = account::login().await?;
                    account_info.save()?;
                } else if items[index] == "Регистрация" {
                    let account_info = account::registration().await?;
                    account_info.save()?;
                }
            }
            None => panic!("Вы не выбрали действие!"),
        }
    }

    account::shell_account().await?;
    Ok(())
}
