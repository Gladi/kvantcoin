table! {
    account (id) {
        id -> Integer,
        email -> Text,
        password -> Text,
        address_wallet -> Text,
        balance -> Integer,
    }
}

#[derive(Insertable)]
#[table_name="account"]
pub struct NewAccount<'a> {
    pub email: &'a str,
    pub password: &'a str,
    pub address_wallet: &'a str,
    pub balance: i32
}