use std::env;
use std::fmt::LowerHex;
use dotenv_codegen::dotenv;
use anyhow::{Result, Context};
use diesel::MysqlConnection;
use diesel::prelude::*;
use diesel::result::Error;
use sha2::{Sha256, Digest};
use crate::server::schema::account::dsl::*;
use crate::server::schema::NewAccount;
use super::models::Account;

macro_rules! check_accounts_is_empty {
    ($accounts:ident) => {
        if $accounts.is_empty() {
            anyhow::bail!("Accounts is empty");
        }
    };
}

pub type AddressWallet = String;
pub type Password = String;
pub type InfoForSendMoney = (AddressWallet, Password);

pub struct DB {
    connect: MysqlConnection
}

impl DB {
    pub fn new() -> Self {
        let address_to_db = env::var("DATABASE_URL").unwrap();
 
        let connection = MysqlConnection::establish(&address_to_db)
        .expect(&format!("Error connecting to {}", address_to_db));

        DB {
            connect: connection.into()
        }
    }

    fn get_password_for_database(&self, rpassword: &str) -> String {
        let salt = dotenv!("GLOBAL_SALT");
        let mut hasher = Sha256::new();
        let mut password_and_salt = String::new();
        password_and_salt.push_str(salt);
        password_and_salt.push_str(rpassword);

        hasher.update(password_and_salt.as_bytes());
        let password_for_database = hasher.finalize();

        format!("{:x}", password_for_database) // LowerHex
    }

    pub fn new_account(&self, raddress_wallet: &str, remail: &str, rpassword: &str, rbalance: i32) -> Result<()> {
        let password_for_database = self.get_password_for_database(rpassword);
        
        let new_account = NewAccount {
            email: remail,
            password: &password_for_database,
            address_wallet: raddress_wallet,
            balance: rbalance
        };

        diesel::insert_into(account).values(new_account).execute(&self.connect)?;

        Ok(())
    }

    pub fn get_account_by_address_wallet(&self, raddress_wallet: &str) -> Result<Account> {
        let accounts = account.filter(address_wallet.eq(raddress_wallet)).limit(1).load::<Account>(&self.connect)?;
        check_accounts_is_empty!(accounts);

        Ok(accounts[0].clone())
    }

    pub fn get_account_by_email(&self, remail: &str) -> Result<Account> {
        let accounts = account.filter(email.eq(remail)).limit(1).load::<Account>(&self.connect)?;
        check_accounts_is_empty!(accounts);

        Ok(accounts[0].clone())
    }

    pub fn send_money(&self, to: InfoForSendMoney, from_address_wallet: String, value: i32) -> Result<()> {
        // TODO Использовать транзакции
        // Но это будет в другой версии, не в 1.0.0

            let (to_address_wallet, to_password) = to;

            let accounts_to = self.get_account_by_address_wallet(to_address_wallet.as_str())?;
            let accounts_from = self.get_account_by_address_wallet(from_address_wallet.as_str())?;

            if value > accounts_to.balance {
                anyhow::bail!("The account 'to' does not have enough money to complete the operation");
            }

            if accounts_to.password != self.get_password_for_database(to_password.as_str()) {
                anyhow::bail!("The account 'to' have incorrect password");
            }
            
            let new_balance_for_to = accounts_to.balance - value;
            let new_balance_for_from = accounts_from.balance + value;

            diesel::update(account.filter(address_wallet.eq(to_address_wallet))).set(balance.eq(new_balance_for_to)).execute(&self.connect)?;
            diesel::update(account.filter(address_wallet.eq(from_address_wallet))).set(balance.eq(new_balance_for_from)).execute(&self.connect)?;

        Ok(())
    }

    pub fn login_to_account(&self, remail: &str, rpassword: &str) -> Result<bool> {
        let account_info = self.get_account_by_email(remail)?;
        Ok(account_info.password == self.get_password_for_database(rpassword))
    }
}

#[cfg(test)]
mod tests {
    use crate::server::db::*; // TODO использовать super

    #[test]
    fn connect_or_new() {
        dotenv::dotenv().unwrap();
        let _database = DB::new();
    }

    #[test]
    fn create_test_account() {
        dotenv::dotenv().unwrap();
        let database = DB::new();

        database.new_account("raddress_wallet32", "remail645", "rpassword345", 0).unwrap();
    }

    #[test]
    fn get_account_by_address_wallet() {
        dotenv::dotenv().unwrap();
        let database = DB::new();

        database.new_account("raddress_wallet5645", "remail6426", "rpassword65425", 0).unwrap();
        let info_account = database.get_account_by_address_wallet("raddress_wallet5645").unwrap();

        assert_eq!(info_account.email, "remail6426")
    }

    #[test]
    fn get_account_by_email() {
        dotenv::dotenv().unwrap();
        let database = DB::new();

        database.new_account("raddress_wallet8367", "remail29563", "rpassword74372", 0).unwrap();
        let info_account = database.get_account_by_email("remail29563").unwrap();

        assert_eq!(info_account.address_wallet, "raddress_wallet8367")
    }

    #[test]
    fn send_money() {
        dotenv::dotenv().unwrap();
        let database = DB::new();

        database.new_account("raddress_wallet_dsvsg", "remail_sdfg", "rpassword_sfgsf", 100).unwrap();
        database.new_account("raddress_wallet_dfhsd", "remail_dfbadb", "rpassword_fdsht", 0).unwrap();
        
        database.send_money(("raddress_wallet_dsvsg".to_string(), "rpassword_sfgsf".to_string()),
                                "raddress_wallet_dfhsd".to_string(), 40).unwrap();
    }

    #[test]
    fn login_to_account() {
        dotenv::dotenv().unwrap();
        let database = DB::new();

        database.new_account("raddress_wallet_6fds1", "remail_6fds1", "rpassword_6fds1", 0).unwrap();
        let result = database.login_to_account("remail_6fds1", "rpassword_6fds1").unwrap();
        assert_eq!(result, true)
    }
}
