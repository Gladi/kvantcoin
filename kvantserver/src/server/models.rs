use diesel::Queryable;

#[derive(Queryable, Clone)]
pub struct Account {
    pub id: i32,
    pub email: String,
    pub password: String,
    pub address_wallet: String,
    pub balance: i32
}