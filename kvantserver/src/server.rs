mod db;
mod models;
mod schema;

use tonic::{transport::Server, Request, Response, Status};
use KvantcoinProtocol::*;
use KvantcoinProtocol::kvantcoin_protocol_server::*;
use log::*;
use std::sync::Mutex;

pub mod KvantcoinProtocol {
    tonic::include_proto!("kvantcoin_proto"); // The string specified here must match the proto package name
}

pub struct MyKvantcoinProtocol {
    database: Mutex<db::DB>
}

impl Default for MyKvantcoinProtocol {
    fn default() -> Self {
        MyKvantcoinProtocol { 
            database: Mutex::new(db::DB::new())
        }
    }
}

#[tonic::async_trait]
impl kvantcoin_protocol_server::KvantcoinProtocol for MyKvantcoinProtocol {
    async fn new_account(
        &self,
        request: Request<NewAccountRequest>, 
    ) -> Result<Response<NewAccountResponse>, Status> {
        let db = self.database.lock().unwrap();
        if db.new_account(&request.get_ref().address_wallet,
        &request.get_ref().email, &request.get_ref().password, 0).is_err() {
            let response = NewAccountResponse {
                is_successful: false,
                message_error: "email or address wallet already in use".to_string()
            };
    
            error!("New transaction failed: new account: {:?}; error message: {}", request.get_ref(), response.message_error);
            return Ok(Response::new(response));
        }

        info!("New transaction successful: new account: {:?}", request.get_ref());
            let response = NewAccountResponse {
                is_successful: true,
                message_error: "No error".to_string()
            };

            Ok(Response::new(response))
    }

    async fn send_money(
        &self,
        request: Request<SendMoneyRequest>, 
    ) -> Result<Response<SendMoneyResponse>, Status> {
        let db = self.database.lock().unwrap();
        if db.send_money((request.get_ref().to.clone(), request.get_ref().password.clone()), request.get_ref().from.clone(), request.get_ref().value).is_err() {
            let response = SendMoneyResponse {
                is_successful: false,
                message_error: "Password or from address incorrect or the account does not have enough money to complete the operation".to_string()
            };
    
            error!("New transaction failed: send money: {:?}; error message: {}", request.get_ref(), response.message_error);
            return Ok(Response::new(response));
        }
        
        info!("New transaction successful: send money: {:?}", request.get_ref());
        let response = SendMoneyResponse {
            is_successful: true,
            message_error: "No error".to_string()
        };

        Ok(Response::new(response))
    }

    async fn login_to_account(
        &self,
        request: Request<LoginToAccountRequest>, 
    ) -> Result<Response<LoginToAccountResponse>, Status> {
        let db = self.database.lock().unwrap();
        let result = db.login_to_account(&request.get_ref().email, &request.get_ref().password);
        
        if result.is_err() {
            let response = LoginToAccountResponse {
                is_successful: false,
                message_error: "Account does not exist or password is incorrect".to_string(),
                account_login_successful: false,
                balance_account: 0,
                address_wallet_account: "".to_string()
            };
    
            error!("New transaction failed: login to account: {:?}; error message: {}", request.get_ref(), response.message_error);
            return Ok(Response::new(response));
        }

        info!("New transaction successful: login to account: {:?}", request.get_ref());
        let info_account = db.get_account_by_email(&request.get_ref().email).unwrap();
        let response = LoginToAccountResponse {
            is_successful: true,
            message_error: "No error".to_string(),
            account_login_successful: result.unwrap(),
            balance_account: info_account.balance,
            address_wallet_account: info_account.address_wallet
        };

        Ok(Response::new(response))
    }
}