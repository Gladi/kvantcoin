#![allow(unused_imports)]

#[macro_use] extern crate diesel;
mod server;

use anyhow::{Context, Result};
use colored::Colorize;
use dotenv::dotenv;
use log::*;
use server::*;
use std::process::exit;
use tonic::transport::Server;

#[tokio::main]
async fn main() -> Result<()> {
    dotenv()?;
    env_logger::init();
    info!("env and logger work!");

    info!("Set ctrlc cook...");
    ctrlc::set_handler(move || {
        warn!("{}", "DISABLE KVANTSERVER...".red());
        exit(0);
    })
    .context("Error setting Ctrl-C handler")?;

    info!("Start Kvantcoin server...");
    let addr: &str = dotenv_codegen::dotenv!("SERVER_ADDRESS");
    let protocol = MyKvantcoinProtocol::default();
    let service =
        KvantcoinProtocol::kvantcoin_protocol_server::KvantcoinProtocolServer::new(protocol);

    let await_server = Server::builder().add_service(service).serve(addr.parse()?);

    info!("{}", "SERVER IS WORK!".red());

    await_server.await?;
    Ok(())
}
